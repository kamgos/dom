using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Movement : MonoBehaviour
{

    [Serializable]
    public class Siku   // parametry siku
    {
        public float IleTrwa = 1.5f;        // Ile czasu trwa czynno�� [s]
        public float level = 0.0f;          // aktualny stan siku
        public float rate = 0.05f;          // wzrost poziomu siku w l/s
        public float maxLevel = 2.0f;       // maksymalny poziom siku w p�cherzu
        public Slider slider = null;        // Suwak od siku
        public float triggerLevel = 1.5f;   // Przy jakim poziomie siku ma i�� do kibla
        public Transform target = null;     // Gdzie ma i�� jak mu si� chce siku
        public AudioClip audioClip = null;  // D�wi�k zwi�zany z czynno�ci�
    }

    [Serializable]
    public class Jedzenie // parametry jedzenia
    {
        public float IleTrwa = 2.5f;       // Ile czasu trwa czynno�� [s]
        public float level = 1.0f;         // aktualny stan najedzenie
        public float rate= -0.05f;         // spadek poziomu najedzenia
        public float maxLevel = 1.0f;      // maksymalny poziom najedzenia
        public Slider slider = null;       // Suwak od jedzenia
        public float triggerLevel = 0.5f;  // Przy jakim poziomie najedzenia ma i�� je��
        public Transform target = null;    // Gdzie ma i�� jak mu si� chce je��
        public AudioClip audioClip = null;  // D�wi�k zwi�zany z czynno�ci�
    }

    [Serializable]
    public class Zabawa // parametry zabawy
    {
        public float IleTrwa = 5.0f;       // Ile czasu trwa czynno�� [s]
        public float level = 3.0f;         // aktualny stan rozrywki
        public float rate = -0.1f;         // spadek poziomu rozrywki
        public float maxLevel = 3.0f;      // maksymalny poziom uchachania
        public Slider slider = null;       // Suwak od rozrywki
        public float triggerLevel = 1.2f;  // Przy jakim poziomie nudy ma si� rozerwa�
        public Transform target = null;    // Gdzie ma i�� jak mu si� chce bawi�
        public AudioClip audioClip = null;  // D�wi�k zwi�zany z czynno�ci�
    }
    
    [Serializable]
    public class Spanko  // parametry senno�ci
    {
        public float IleTrwa = 10.0f;      // Ile czasu trwa czynno�� [s]
        public float level = 3.0f;         // aktualny stan wyspania
        public float rate = -0.1f;         // spadek poziomu wyspania
        public float maxLevel = 3.0f;      // maksymalny poziom wyspania
        public Slider slider = null;       // Suwak od wyspania
        public float triggerLevel = 1.2f;  // Przy jakim poziomie senno�ci ma i�� spa�
        public Transform target = null;    // Gdzie ma i�� jak mu si� chce spa�
        public AudioClip audioClip = null;  // D�wi�k zwi�zany z czynno�ci�
    }

    [Serializable]
    public class Cwiczenie  // parametry senno�ci
    {
        public float IleTrwa = 10.0f;      // Ile czasu trwa czynno�� [s]
        public float level = 3.0f;         // aktualny poziom �wiecze�
        public float rate = -0.1f;         // spadek poziomu wy�iwczenia
        public float maxLevel = 3.0f;      // maksymalny poziom wy�wiczenia
        public Slider slider = null;       // Suwak od �wiczenia
        public float triggerLevel = 1.2f;  // Przy jakim poziomie wy�wiczenia ma i�� �wiczy�
        public Transform target = null;    // Gdzie ma i�� jak mu si� chce �wiczy�
        public AudioClip audioClip = null;  // D�wi�k zwi�zany z czynno�ci�
    }

    public Siku siku;
    public Jedzenie jedzenie;
    public Zabawa zabawa;
    public Spanko spanko;
    public Cwiczenie cwiczenie;

    public enum Stany { Siku, Idle, Jedzenie, Zabawa, Sen, Cwiczenie };   // Mo�liwe stany delikwenta
    public Stany stan = Stany.Idle;                    // Domy�lnie nic nie robi
    public float biezacaCzynnoscTime = 0.0f;  

    private NavMeshAgent agent;
    private Animator animator;
    private AudioSource audioSource;
    
    public float stopDistance = 0.1f;                   // W jakiej odleg�o�ci od celu uznajemy, �e ju� doszed�
    public Transform idleTarget = null;                 // miejsce, gdzie ma nic nie robi�
    public AudioClip walkAudioClip = null;              // D�wi�k tuptania

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        audioSource = GetComponent<AudioSource>();

        // ustawienie pocz�tkowego poziomu siku itp.
        siku.slider.value = siku.maxLevel - siku.level;
        siku.slider.maxValue = siku.maxLevel - siku.level;

        // ustawienie pocz�tkowego poziomu jedzenia
        jedzenie.slider.value = jedzenie.level;
        jedzenie.slider.maxValue = jedzenie.maxLevel;

        // ustawienie pocz�tkowego poziomu rozrywki
        zabawa.slider.value = zabawa.level;
        zabawa.slider.maxValue = zabawa.maxLevel;

        // ustawienie pocz�tkowego poziomu senno�ci
        spanko.slider.value = spanko.level;
        spanko.slider.maxValue = spanko.maxLevel;

        // ustawienie pocz�tkowego poziomu senno�ci
        cwiczenie.slider.value = cwiczenie.level;
        cwiczenie.slider.maxValue = cwiczenie.maxLevel;

        // wy�lij typa na pozycj� nic nierobienia
        GoIdle();
    }

    void Update()
    {

        AktualizujLevele();
        UstawKoloryPaskow();

        if (stan != Stany.Idle && biezacaCzynnoscTime > 0.0f)          // je�li jest czym� zaj�ty to nie dostanie nowego zadania
        {
            biezacaCzynnoscTime -= Time.deltaTime;
            if (biezacaCzynnoscTime <= 0.0f)
            {
                biezacaCzynnoscTime = 0.0f;
                GoIdle();
            }
            return;
        }

        if (siku.level >= siku.triggerLevel && stan==Stany.Idle)    // je�li poziom siku jest powy�ej trigger levelu
        {
            IdzSiku();
        };

        if (jedzenie.level <= jedzenie.triggerLevel && stan==Stany.Idle)
        {
            IdzJesc();
        }

        if (zabawa.level <= zabawa.triggerLevel && stan == Stany.Idle)
        {
            IdzBawSie();
        }

        if (spanko.level <= spanko.triggerLevel && stan == Stany.Idle)
        {
            IdzDoSypialni();
        }

        if (cwiczenie.level <= cwiczenie.triggerLevel && stan == Stany.Idle)
        {
            IdzCwiczyc();
        }

        CzyDoralDoCelu();
    }

    private void GoIdle()
    {
        stan = Stany.Idle;
        agent.SetDestination(idleTarget.position);
        animator.SetBool("isWalking", true);
        animator.SetBool("isSleeping", false);
        animator.SetBool("isPickingUp", false);
        animator.SetBool("isCrazy", false);
        animator.SetBool("isPlotting", false);
        animator.SetBool("isExcercising", false);
        audioSource.clip = walkAudioClip;
        audioSource.Play();
    }

    private void IdzDoSypialni()
    {        
        agent.SetDestination(spanko.target.position);   // wy�lij do sypialni
        stan = Stany.Sen;                               // Zmien stan na spanko
        animator.SetBool("isWalking", true);
        audioSource.clip = walkAudioClip;               // Odtwarzaj d�wi�k tuptania
        audioSource.Play();
    }

    private void IdzBawSie()
    {
        agent.SetDestination(zabawa.target.position);       // wy�lij do bawialni
        stan = Stany.Zabawa;                                // Zmien stan na zabaw�
        animator.SetBool("isWalking", true);
        audioSource.clip = walkAudioClip;                   // Odtwarzaj d�wi�k tuptania
        audioSource.Play();
    }

    private void IdzJesc()
    {
        agent.SetDestination(jedzenie.target.position);     // wy�lij do kuchni           
        stan = Stany.Jedzenie;                              // Zmie� stan na jedzenie
        animator.SetBool("isWalking", true);
        audioSource.clip = walkAudioClip;                   // Odtwarzaj d�wi�k tuptania
        audioSource.Play();
    }

    private void IdzSiku()
    {
        agent.SetDestination(siku.target.position);         // wy�lij do �azienki
        stan = Stany.Siku;                                  // zmie� stan na siku
        animator.SetBool("isWalking", true);
        audioSource.clip = walkAudioClip;                   // Odtwarzaj d�wi�k tuptania
        audioSource.Play();
    }
    private void IdzCwiczyc()
    {
        agent.SetDestination(cwiczenie.target.position);    // wy�lij �wiczy�
        stan = Stany.Cwiczenie;                             // zmie� stan na �wiczenie
        animator.SetBool("isisExcercising", true);
        audioSource.clip = walkAudioClip;                   // Odtwarzaj d�wi�k tuptania
        audioSource.Play();
    }

    private void CzyDoralDoCelu()
    {
        // Sprawd� czy �cie�ka ju� obliczona
        if (agent.pathPending || agent.remainingDistance == Mathf.Infinity) return;

        if (agent.remainingDistance < stopDistance)
        {
            animator.SetBool("isWalking", false);
            audioSource.Stop();
            switch (stan)
            {
                case Stany.Siku:
                    ZrobSiku();
                    break;

                case Stany.Jedzenie:
                    Jedz();
                    break;

                case Stany.Zabawa:
                    BawSie();
                    break;

                case Stany.Sen:
                    Spij();
                    break;

                case Stany.Cwiczenie:
                    Cwicz();
                    break;

                default: break;
            }
        }
    }

    private void AktualizujLevele()
    {
        // Aktualizacja poziomu siku
        siku.level += siku.rate * Time.deltaTime;
        siku.slider.value = siku.maxLevel - siku.level;

        // Aktualizacja poziomu najedzenia
        jedzenie.level += jedzenie.rate * Time.deltaTime;
        jedzenie.slider.value = jedzenie.level;

        // Aktualizacja poziomu zabawy
        zabawa.level += zabawa.rate * Time.deltaTime;
        zabawa.slider.value = zabawa.level;

        // Aktualizacja poziomu senno�ci
        spanko.level += spanko.rate * Time.deltaTime;
        spanko.slider.value = spanko.level;

        // Aktualizacja poziomu wy�wiczenia
        cwiczenie.level += cwiczenie.rate * Time.deltaTime;
        cwiczenie.slider.value = cwiczenie.level;
    }

    private IEnumerator PlaySoundAfterDelay(float delay, AudioClip audioClip, bool oneShot=false)
    {
        // Wait for the specified delay
        yield return new WaitForSeconds(delay);
        
        // Play the sound clip
        if (oneShot)
        {
            audioSource.PlayOneShot(audioClip);
        }
        else
        {            
            audioSource.clip = audioClip;
            audioSource.Play();
        }        
    }

    private void ZrobSiku()
    {
        siku.level = 0.0f;
        biezacaCzynnoscTime = siku.IleTrwa;
        siku.slider.fillRect.GetComponent<Image>().color = Color.white;
        animator.SetBool("isWalking", false);
        animator.SetBool("isPlotting", true);       
        StartCoroutine(PlaySoundAfterDelay(2.0f, siku.audioClip, true));
    }

    private void Jedz()
    {
        jedzenie.level = jedzenie.maxLevel;
        biezacaCzynnoscTime = jedzenie.IleTrwa;
        jedzenie.slider.fillRect.GetComponent<Image>().color = Color.white;
        animator.SetBool("isWalking", false);
        animator.SetBool("isPickingUp", true);        
        StartCoroutine(PlaySoundAfterDelay(2.5f, jedzenie.audioClip, true));
    }

    private void BawSie()
    {
        zabawa.level = zabawa.maxLevel;
        biezacaCzynnoscTime = zabawa.IleTrwa;
        zabawa.slider.fillRect.GetComponent<Image>().color = Color.white;
        animator.SetBool("isWalking", false);
        animator.SetBool("isCrazy", true);
        StartCoroutine(PlaySoundAfterDelay(1.0f, zabawa.audioClip));
    }

    private void Spij()
    {
        spanko.level = spanko.maxLevel;
        biezacaCzynnoscTime = spanko.IleTrwa;
        spanko.slider.fillRect.GetComponent<Image>().color = Color.white;
        animator.SetBool("isWalking", false);
        animator.SetBool("isSleeping", true);
        StartCoroutine(PlaySoundAfterDelay(2.0f, spanko.audioClip));
    }

    private void Cwicz()
    {
        cwiczenie.level = cwiczenie.maxLevel;
        biezacaCzynnoscTime = cwiczenie.IleTrwa;
        cwiczenie.slider.fillRect.GetComponent<Image>().color = Color.white;
        animator.SetBool("isWalking", false);
        animator.SetBool("isExcercising", true);
        StartCoroutine(PlaySoundAfterDelay(1.0f, cwiczenie.audioClip));
    }

    private void UstawKoloryPaskow()
    {
        // Ustaw kolory pask�w jak jest blisko kryzysu
        if (siku.level >= siku.triggerLevel)
        {
            siku.slider.fillRect.GetComponent<Image>().color = Color.yellow;
        };

        if (jedzenie.level <= jedzenie.triggerLevel)
        {
            jedzenie.slider.fillRect.GetComponent<Image>().color = Color.yellow;
        }

        if (zabawa.level <= zabawa.triggerLevel)
        {
            zabawa.slider.fillRect.GetComponent<Image>().color = Color.yellow;
        }

        if (spanko.level <= spanko.triggerLevel)
        {
            spanko.slider.fillRect.GetComponent<Image>().color = Color.yellow;
        }

        if (cwiczenie.level <= cwiczenie.triggerLevel)
        {
            cwiczenie.slider.fillRect.GetComponent<Image>().color = Color.yellow;
        }
    }
}