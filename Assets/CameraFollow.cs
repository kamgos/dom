using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform player; // Obiekt gracza, za kt�rym kamera ma pod��a�
    public Vector3 offset; // Offset (przesuni�cie) kamery wzgl�dem gracza

    // Update is called once per frame
    void FixedUpdate()
    {
        if (player != null)
        {
            transform.position = player.position + offset;
        }
    }
}